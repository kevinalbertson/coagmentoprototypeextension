var self = require('sdk/self')
	, config = require('config')
	, ui = require('sdk/ui')
	, { Toolbar } = require('sdk/ui/toolbar')
	, { Sidebar } = require('sdk/ui/sidebar')
	, { Frame } = require('sdk/ui/frame')
	, api = require('lib/api')
	, tabutils = require("sdk/tabs/utils")
	, windowutils = require("sdk/window/utils")
	;

var sidebar = ui.Sidebar({
	id: 'coagmento-sidebar',
	title: 'Coagmento Sidebar',
	url: require('sdk/self').data.url('sidebar.html')
});

sidebar.show();

var frame = ui.Frame({
	url: require('sdk/self').data.url('toolbar.html'),
	onMessage: onToolbarMessage
});

var toolbar = ui.Toolbar({
	title: 'Coagmento Toolbar',
	items: [frame]
});

// Listen for save bookmark requests from the frame.
function onToolbarMessage(e) {
	if(e.data.action == "save_bookmark") {
		console.log("Saving bookmark");
		api.saveBookmark({
			url: getCurrentURL(),
			title: getCurrentTitle()
		});
	}
}

function getCurrentURL(){
    var res = tabutils.getTabURL(tabutils.getActiveTab(windowutils.getMostRecentBrowserWindow()));
    return encodeURIComponent(res);
}

function getCurrentTitle(){
    var res = tabutils.getTabTitle(tabutils.getActiveTab(windowutils.getMostRecentBrowserWindow()));
    return encodeURIComponent(res);
}
